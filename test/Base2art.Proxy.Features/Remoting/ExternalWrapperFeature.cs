﻿namespace Base2art.Remoting
{
    using System;
    using System.IO;
    using System.Linq;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class ExternalWrapperFeature
    {
        private const string Config =
            #if DEBUG
            "debug";
        #else
        "release";
        #endif
        
        
        [Test]
        public void ShouldHandleValidationIssues()
        {
            Action mut = () => new DynamicProxy<ITickProvider>(null);
            mut.ShouldThrow<ArgumentNullException>();
        }
        
        [Test]
        public void ShouldHandleNullRelativeParts()
        {
            var obj = ExternalProxyObject.CreateWrapper<ITickProvider>(
                asm => Activator.CreateInstance(asm.GetType("Base2art.Proxy.Fixtures.TickProvider")),
                ((string[])null));
            Assert.Throws<FileNotFoundException>(() => obj.Object.TickCount.ToString()).Message.Should().NotContain("''")
                .And.Subject.Count(x=>x == '\'').Should().Be(6);
        }
        
        [Test]
        public void ShouldLoadWithHappyPath()
        {
            var obj = ExternalProxyObject.CreateWrapper<ITickProvider>(
                asm => Activator.CreateInstance(asm.GetType("Base2art.Proxy.Fixtures.TickProvider")),
                 "Base2art.Proxy.Features.Fixtures.dll"
               );
            obj.Object.TickCount.Should().BeGreaterThan(10);
            object.ReferenceEquals(obj.Object, obj.Object);
            
            var obj2 = ExternalProxyObject.Create<ITickProvider>(
                asm => Activator.CreateInstance(asm.GetType("Base2art.Proxy.Fixtures.TickProvider")),
                 "Base2art.Proxy.Features.Fixtures.dll"
               );
            obj2.TickCount.Should().BeGreaterThan(10);
        }
        
        [Test]
        public void ShouldLoadWithHappyPathWithPrep()
        {
            var obj = ExternalProxyObject.CreateWrapper<ITickProvider>(
                asm => Activator.CreateInstance(asm.GetType("Base2art.Proxy.Fixtures.TickProvider")),
                 "Base2art.Proxy.Features.Fixtures.dll"
               );
            
            obj.Prep += (object sender, PrepItemEventArgs args) => { args.ItemToPrep.Value= 10; };
            obj.Object.TickCount.Should().Be(10);
            
            object.ReferenceEquals(obj.Object, obj.Object);
        }
        
        [Test]
        public void ShouldHandleExceptionAppropriately()
        {
            //"..", "..", "..", "Base2art.BCL.Features.Fixtures", "bin", Config,
            var obj = ExternalProxyObject.CreateWrapper<ITickProvider>(
                asm => Activator.CreateInstance(asm.GetType("Base2art.Proxy.Fixtures.TickProvider")),
                 "Base2art.Proxy.Features.Fixtures.dll"
               );
            
            Assert.Throws<MissingMethodException>(() => obj.Object.FakeProp.ToString());
        }
        
        
        public interface ITickProvider
        {
            int TickCount {get;}
            int FakeProp {get;}
        }
    }
}
