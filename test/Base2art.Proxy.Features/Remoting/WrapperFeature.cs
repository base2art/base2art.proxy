﻿namespace Base2art.Remoting
{
    using System;
    using System.Linq;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WrapperFeature
    {
        [Test]
        public void ShouldHandleNullRelativeParts()
        {
            ProxyObject.Create<ITickProvider>(new TickProvider());
        }
        
        [Test]
        public void ShouldLoadWithHappyPath()
        {
            var obj = ProxyObject.CreateWrapper<ITickProvider>(new TickProvider());
            obj.Object.TickCount.Should().BeGreaterThan(10);
            object.ReferenceEquals(obj.Object, obj.Object);
        }
        
        [Test]
        public void ShouldLoadWithHappyPathWithPrep()
        {
            var obj = ProxyObject.CreateWrapper<ITickProvider>(new TickProvider());
            
            obj.Prep += (object sender, PrepItemEventArgs args) => { args.ItemToPrep.Value= 10; };
            obj.Object.TickCount.Should().Be(10);
            
            object.ReferenceEquals(obj.Object, obj.Object);
        }
        
        [Test]
        public void ShouldHandleExcpetionAppropriately()
        {
            var obj = ProxyObject.CreateWrapper<ITickProvider>(new TickProvider());
            
            Assert.Throws<MissingMethodException>(() => obj.Object.FakeProp.ToString());
        }
        
        
        
        private class TickProvider
        {
            public int TickCount
            {
                get
                {
                    if (!this.Value.HasValue)
                    {
                        return Environment.TickCount;
                    }
                    
                    return this.Value.Value;
                }
            }
            
            public int? Value
            {
                get; set;
            }
        }
        
        private interface ITickProvider
        {
            int TickCount {get;}
            int FakeProp {get;}
        }
    }
}


