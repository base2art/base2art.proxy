﻿namespace Base2art.Fixtures
{
    using System;

    public class ClonablePerson : IPerson, ICloneable
    {
        public string Name
        {
            get;
            set;
        }

        public string[] Aliases
        {
            get;
            set;
        }

        public DateTime Birthday
        {
            get;
            set;
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public ClonablePerson Clone()
        {
            return (ClonablePerson)this.MemberwiseClone();
        }
    }
}
