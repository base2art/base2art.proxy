﻿namespace Base2art.Fixtures
{
	public enum SchoolLevelData
	{
		MiddleSchool,
		HighSchool,
		College,
		PHD
	}
}
