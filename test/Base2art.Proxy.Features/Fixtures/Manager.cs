﻿namespace Base2art.Fixtures
{
    public class Manager
    {
        public IEmployee EmployeeInformation { get; set ; }
        
        public IPerson PersonalInformation { get; set ; }
        
        public string[] TeamNames { get; set; }
        
        public IEmployee[] DirectReports { get; set ; }
        
        public IPerson[] FrendsAtWork { get; set ; }
        
        public IPerson[] Children { get; set ; }
    }
}
