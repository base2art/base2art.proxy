﻿namespace Base2art.Fixtures
{
	using System;

	public interface IEmployee : IPerson
	{
		int EmployeeNumber
		{
			get;
		}
	}
}

