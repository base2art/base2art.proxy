﻿namespace Base2art
{
    using System;
    
    internal static class Validation
    {
        internal static void ValidateIsNotNull<T>(this T value)
            where T : class
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
        }
    }
}
