﻿namespace Base2art.Remoting
{
    using System;
    
    public class ProxyObjectWrapper<T> : ProxyObjectWrapperBase<T>
    {
        public ProxyObjectWrapper(object value) : base(value)
        {
        }
        
        public event EventHandler<PrepItemEventArgs> Prep;

        public T Object
        {
            get { return this.TargetObject; }
        }
        
        protected override void PrepAfterCreation(dynamic backValue)
        {
            var item = this.Prep;
            if (item != null)
            {
                item.Invoke(this, new PrepItemEventArgs(backValue));
            }
        }
    }
}
