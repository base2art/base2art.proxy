﻿namespace Base2art.Remoting
{
    using System;
    
    public class PrepItemEventArgs : EventArgs
    {
        private readonly object value;

        public PrepItemEventArgs(object value)
        {
            this.value = value;
        }

        public dynamic ItemToPrep
        {
            get
            {
                return this.value;
            }
        }
    }
}
