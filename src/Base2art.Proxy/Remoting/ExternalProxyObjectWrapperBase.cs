﻿namespace Base2art.Remoting
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security;
    using System.Security.Permissions;
    
    public abstract class ExternalProxyObjectWrapperBase<T>
    {
        private readonly object padlock = new object();

        private DynamicProxy<T> backValue;
        
        protected T TargetObject
        {
            [SecurityCritical]
            get
            {
                if (this.backValue != null)
                {
                    return this.Proxy;
                }
                
                lock (this.padlock)
                {
                    if (this.backValue == null)
                    {
                        var path = this.GetDllPath(this.RelativePathParts());
                        var asm = Assembly.LoadFile(path);
                        
                        var value = this.CreateObject(asm);
                        this.PrepAfterCreation(value);
                        this.backValue = new DynamicProxy<T>(value);
                    }
                    
                    return this.Proxy;
                }
            }
        }

        private T Proxy
        {
            get
            {
                return (T)this.backValue.GetTransparentProxy();
            }
        }

        protected virtual Assembly FindContainingAssembly()
        {
            return this.GetType().Assembly;
        }
        
        protected abstract void PrepAfterCreation(dynamic backValue);
        
        protected abstract object CreateObject(Assembly assembly);

        protected abstract string[] RelativePathParts();
        
        [SecurityCritical]
        protected virtual string EntryPointDir()
        {
            return RetrieveEntryPointDir();
        }

        protected virtual string LocationDir(Assembly assembly)
        {
            return Path.GetDirectoryName(assembly.Location);
        }

        protected virtual string CodeBaseDir(Assembly assembly)
        {
            var uri = new Uri(assembly.CodeBase);
            var path = uri.AbsolutePath;
            return Path.GetDirectoryName(path);
        }

        private static Tuple<bool, string> Combine(string dirName, string[] dirs)
        {
            var dllPath = dirName;
            
            foreach (var dir in dirs)
            {
                dllPath = Path.Combine(dllPath, dir);
            }
            
            if (File.Exists(dllPath))
            {
                return Tuple.Create(true, dllPath);
            }
            
            return Tuple.Create(false, dllPath);
        }
        
        [SecurityCritical]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands", Justification = "SjY")]
        private static string RetrieveEntryPointDir()
        {
            using (var process = System.Diagnostics.Process.GetCurrentProcess())
            {
                using (var mainModule = process.MainModule)
                {
                    return Path.GetDirectoryName(mainModule.FileName);
                }
            }
        }
        
        [SecurityCritical]
        private string GetDllPath(string[] dirs)
        {
            var mainModuleDirectory = this.EntryPointDir();
            var path = Combine(mainModuleDirectory, dirs);
            List<string> coll = new List<string>();
            if (!path.Item1)
            {
                coll.Add(path.Item2);
                var asm = this.FindContainingAssembly();
                var location = this.LocationDir(asm);
                path = Combine(location, dirs);
                if (!path.Item1)
                {
                    coll.Add(path.Item2);
                    var codebase = this.CodeBaseDir(asm);
                    path = Combine(codebase, dirs);
                    if (!path.Item1)
                    {
                        coll.Add(path.Item2);
                        var input = string.Join(",\n ", coll.Select(x => string.Concat("'", x, "'")));
                        var message = string.Format(CultureInfo.InvariantCulture, "Unable To Load Assembly; Searched {0}", input);
                        throw new FileNotFoundException(message);
                    }
                }
            }
            
            return path.Item2;
        }
    }
}
