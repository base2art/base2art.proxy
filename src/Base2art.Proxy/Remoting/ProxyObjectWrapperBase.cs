﻿namespace Base2art.Remoting
{
    public abstract class ProxyObjectWrapperBase<T>
    {
        private readonly object value;

        private readonly object padlock = new object();

        private DynamicProxy<T> backValue;

        protected ProxyObjectWrapperBase(object value)
        {
            value.ValidateIsNotNull();
            
            this.value = value;
        }

        protected T TargetObject
        {
            get
            {
                if (this.backValue != null)
                {
                    return this.Proxy;
                }
                
                lock (this.padlock)
                {
                    if (this.backValue == null)
                    {
                        this.PrepAfterCreation(this.value);
                        this.backValue = new DynamicProxy<T>(this.value);
                    }
                    
                    return this.Proxy;
                }
            }
        }

        private T Proxy
        {
            get
            {
                return (T)this.backValue.GetTransparentProxy();
            }
        }
        
        protected abstract void PrepAfterCreation(dynamic backValue);
    }
}
