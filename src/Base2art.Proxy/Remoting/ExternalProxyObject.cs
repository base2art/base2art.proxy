﻿namespace Base2art.Remoting
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Security;

    public static class ExternalProxyObject
    {
        [SecurityCritical]
        public static T Create<T>(Func<Assembly, object> creationFunction, params string[] pathParts)
        {
            return CreateWrapper<T>(creationFunction, pathParts).Object;
        }
        
        public static ExternalProxyObjectWrapper<T> CreateWrapper<T>(Func<Assembly, object> creationFunction, params string[] pathParts)
        {
            return new ExternalProxyObjectWrapper<T>(creationFunction, pathParts);
        }
    }
}
