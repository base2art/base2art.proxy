﻿namespace Base2art.Remoting
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Remoting.Messaging;
    using System.Runtime.Remoting.Proxies;

    public class DynamicProxy<TInterface> : RealProxy
    {
        private readonly object obj;

        public DynamicProxy(object value) : base(typeof(TInterface))
        {
            value.ValidateIsNotNull();
            
            this.obj = value;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "SjY")]
        public override IMessage Invoke(IMessage msg)
        {
            var methodCall = (IMethodCallMessage)msg;
            var methodInfo = methodCall.MethodBase;
            try
            {
                var name = methodInfo.Name;
                var parms = methodInfo.GetParameters().Select(x => x.ParameterType).ToArray();
                var objAsType = this.obj as Type;
                
                Type type = objAsType ?? this.obj.GetType();
                
                var mi = type.GetMethod(methodInfo.Name, parms);
                if (mi == null)
                {
                    throw new MissingMethodException(type.Name, name);
                }
                
                var rez = mi.Invoke(objAsType == null ? this.obj : null, methodCall.InArgs);
                return new ReturnMessage(rez, null, 0, methodCall.LogicalCallContext, methodCall);
            }
            catch (Exception e)
            {
                return new ReturnMessage(e, methodCall);
            }
        }
    }
}
