﻿namespace Base2art.Remoting
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Security;
    
    public class ExternalProxyObjectWrapper<T> : ExternalProxyObjectWrapperBase<T>
    {
        private readonly Func<Assembly, object> creationFunction;

        private readonly string[] pathParts;
        
        public ExternalProxyObjectWrapper(
            Func<Assembly, object> creationFunction,
            params string[] pathParts)
        {
            creationFunction.ValidateIsNotNull();
            
            this.creationFunction = creationFunction;
            this.pathParts = (pathParts ?? new string[0]).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        }
        
        public event EventHandler<PrepItemEventArgs> Prep;

        public T Object
        {
            [SecurityCritical]
            get { return this.TargetObject; }
        }
        
        protected override void PrepAfterCreation(dynamic backValue)
        {
            var item = this.Prep;
            if (item != null)
            {
                item(this, new PrepItemEventArgs(backValue));
            }
        }
        
        protected override object CreateObject(Assembly assembly)
        {
            return this.creationFunction(assembly);
        }
        
        protected override string[] RelativePathParts()
        {
            return this.pathParts;
        }
    }
}