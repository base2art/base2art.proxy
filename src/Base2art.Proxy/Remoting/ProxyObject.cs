﻿namespace Base2art.Remoting
{
    public static class ProxyObject
    {
        public static T Create<T>(object value)
            where T : class
        {
            return CreateWrapper<T>(value).Object;
        }
        
        public static ProxyObjectWrapper<T> CreateWrapper<T>(object value)
            where T : class
        {
            return new ProxyObjectWrapper<T>(value);
        }
    }
}
